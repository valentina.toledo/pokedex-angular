import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicioPokemonService } from '../../servicios/servicio-pokemon.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    ServicioPokemonService
  ]
})
export class PokemonIModule { }
