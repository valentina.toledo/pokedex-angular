import { PokemonI } from '../../modulo/pokemon.model'
import { Component, OnInit } from '@angular/core';
import { ServicioPokemonService } from '../../servicios/servicio-pokemon.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  data:any[] = [];
  

  constructor( private servicioPokemon: ServicioPokemonService, private router: Router) { }

  ngOnInit(): void {
    this.getAllPokemon();
  }

  public getAllPokemon(): void {
    // this.servicioPokemon.getAllpokemon().subscribe(data => {
    //   this.pokemonesGroup = data
    //   console.log( "aca esta", this.pokemonesGroup)    
    // })
    
    let pokemonData;

    for (let i:any = 1; i <= 1180; i++){
      this.servicioPokemon.getPokemones(i).subscribe(
        data => {
        pokemonData = {
          position: i,
          image: data.sprites.front_default,
          name: data.name,
          id: data.id
        };
        this.data.push(pokemonData);
        console.log("arreglo con pooke", data)
      })
    }
  }

  public getDetailPokemon() {
    this.router.navigateByUrl("/detail");
  }

  
}
