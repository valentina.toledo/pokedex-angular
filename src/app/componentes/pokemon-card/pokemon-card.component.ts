import { PokemonI } from '../../modulo/pokemon.model';
import { ServicioPokemonService } from '../../servicios/servicio-pokemon.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.css']
})
export class PokemonCardComponent implements OnInit {

  //@Output() submit = new EventEmitter<string>();
  
  pokemonesGroup:any[] = [] ;
  pokemonDetail:any;
  
  constructor( private servicioPokemon: ServicioPokemonService ) {
    //this.pokemonGroup = this.servicioPokemon.getPokemon();
   }

  ngOnInit(): void {    
    this.getDetailPokemon()
  }

  public getDetailPokemon(): void {
    this.servicioPokemon.getPokemon().subscribe(
      data => {
      this.pokemonDetail = {
        image: data.sprites.front_default,
        name: data.name,
        id: data.id,
        abilities: data.abilities,
        type: data.types,
        height: data.height,
        move: data.moves
      };
      console.log('debeia mostrar: ', this.pokemonDetail)    
    })

  }
  

}