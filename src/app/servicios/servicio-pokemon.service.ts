import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PokemonI } from '../modulo/pokemon.model'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ServicioPokemonService {
  public pokemonId: number = 132;
  public uri: string = 'https://pokeapi.co/api/v2/pokemon/';
  

  constructor( private http: HttpClient) { 
    console.log('servicio funcionando');
  }
  getPokemon() {
    // let urlPokemon = this.http.get( this.uri + this.pokemonId);
    return this.http.get<any>(this.uri + this.pokemonId);
  }

  getPokemones(index:any) {
    return this.http.get<any>(this.uri + index)
  }

  // getAllpokemon():Observable<PokemonI>{
  //   let urlAllpokemon = this.uri;
  //   return this.http.get<PokemonI>(urlAllpokemon);
  // }
}
