import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './componentes/dashboard/dashboard.component';
import { ErrorComponent } from './componentes/error/error.component';
import { PokemonCardComponent } from './componentes/pokemon-card/pokemon-card.component';

const routes: Routes = [
  {path: '', component: DashboardComponent },
  {path: 'detail', component: PokemonCardComponent},
  { path: '**', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
